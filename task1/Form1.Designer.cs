﻿namespace task1
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            contextMenuStrip1 = new ContextMenuStrip(components);
            menuStrip1 = new MenuStrip();
            отобразитьToolStripMenuItem = new ToolStripMenuItem();
            категорииToolStripMenuItem = new ToolStripMenuItem();
            продуктыToolStripMenuItem = new ToolStripMenuItem();
            ключевыеСловаToolStripMenuItem = new ToolStripMenuItem();
            добавитьToolStripMenuItem = new ToolStripMenuItem();
            категориюToolStripMenuItem = new ToolStripMenuItem();
            продукциюToolStripMenuItem = new ToolStripMenuItem();
            ключевыеСловаToolStripMenuItem1 = new ToolStripMenuItem();
            редактироватьКатегориюToolStripMenuItem = new ToolStripMenuItem();
            редактироватьПродуктToolStripMenuItem = new ToolStripMenuItem();
            редактироватьКлсловоToolStripMenuItem = new ToolStripMenuItem();
            dataGridView1 = new DataGridView();
            tabControl1 = new TabControl();
            tabPage1 = new TabPage();
            tabPage2 = new TabPage();
            button1 = new Button();
            comboBox2 = new ComboBox();
            label3 = new Label();
            label2 = new Label();
            label1 = new Label();
            textBox2 = new TextBox();
            textBox1 = new TextBox();
            tabPage5 = new TabPage();
            textBox6 = new TextBox();
            textBox5 = new TextBox();
            button2 = new Button();
            label10 = new Label();
            label9 = new Label();
            label8 = new Label();
            comboBox6 = new ComboBox();
            comboBox5 = new ComboBox();
            comboBox4 = new ComboBox();
            label7 = new Label();
            label6 = new Label();
            textBox4 = new TextBox();
            textBox3 = new TextBox();
            label4 = new Label();
            label5 = new Label();
            tabPage3 = new TabPage();
            button3 = new Button();
            label11 = new Label();
            label12 = new Label();
            comboBox1 = new ComboBox();
            comboBox3 = new ComboBox();
            tabPage4 = new TabPage();
            button4 = new Button();
            tabPage6 = new TabPage();
            comboBox12 = new ComboBox();
            label25 = new Label();
            label26 = new Label();
            button6 = new Button();
            comboBox11 = new ComboBox();
            label22 = new Label();
            label23 = new Label();
            label24 = new Label();
            textBox10 = new TextBox();
            tabPage7 = new TabPage();
            label21 = new Label();
            label20 = new Label();
            comboBox10 = new ComboBox();
            button5 = new Button();
            textBox7 = new TextBox();
            textBox8 = new TextBox();
            label13 = new Label();
            label14 = new Label();
            label15 = new Label();
            comboBox7 = new ComboBox();
            comboBox8 = new ComboBox();
            comboBox9 = new ComboBox();
            label16 = new Label();
            label17 = new Label();
            textBox9 = new TextBox();
            label18 = new Label();
            label19 = new Label();
            tabPage8 = new TabPage();
            button7 = new Button();
            label29 = new Label();
            label30 = new Label();
            comboBox13 = new ComboBox();
            comboBox14 = new ComboBox();
            label27 = new Label();
            label28 = new Label();
            openFileDialog1 = new OpenFileDialog();
            menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)dataGridView1).BeginInit();
            tabControl1.SuspendLayout();
            tabPage1.SuspendLayout();
            tabPage2.SuspendLayout();
            tabPage5.SuspendLayout();
            tabPage3.SuspendLayout();
            tabPage4.SuspendLayout();
            tabPage6.SuspendLayout();
            tabPage7.SuspendLayout();
            tabPage8.SuspendLayout();
            SuspendLayout();
            // 
            // contextMenuStrip1
            // 
            contextMenuStrip1.Name = "contextMenuStrip1";
            contextMenuStrip1.Size = new Size(61, 4);
            // 
            // menuStrip1
            // 
            menuStrip1.Items.AddRange(new ToolStripItem[] { отобразитьToolStripMenuItem, добавитьToolStripMenuItem, редактироватьКатегориюToolStripMenuItem, редактироватьПродуктToolStripMenuItem, редактироватьКлсловоToolStripMenuItem });
            menuStrip1.Location = new Point(0, 0);
            menuStrip1.Name = "menuStrip1";
            menuStrip1.Size = new Size(1351, 30);
            menuStrip1.TabIndex = 1;
            menuStrip1.Text = "menuStrip1";
            // 
            // отобразитьToolStripMenuItem
            // 
            отобразитьToolStripMenuItem.DropDownItems.AddRange(new ToolStripItem[] { категорииToolStripMenuItem, продуктыToolStripMenuItem, ключевыеСловаToolStripMenuItem });
            отобразитьToolStripMenuItem.Font = new Font("Cambria", 14.25F, FontStyle.Regular, GraphicsUnit.Point);
            отобразитьToolStripMenuItem.Name = "отобразитьToolStripMenuItem";
            отобразитьToolStripMenuItem.Size = new Size(124, 26);
            отобразитьToolStripMenuItem.Text = "Отобразить";
            // 
            // категорииToolStripMenuItem
            // 
            категорииToolStripMenuItem.Name = "категорииToolStripMenuItem";
            категорииToolStripMenuItem.Size = new Size(222, 26);
            категорииToolStripMenuItem.Text = "Категории";
            категорииToolStripMenuItem.Click += категорииToolStripMenuItem_Click;
            // 
            // продуктыToolStripMenuItem
            // 
            продуктыToolStripMenuItem.Name = "продуктыToolStripMenuItem";
            продуктыToolStripMenuItem.Size = new Size(222, 26);
            продуктыToolStripMenuItem.Text = "Продукты";
            продуктыToolStripMenuItem.Click += продуктыToolStripMenuItem_Click;
            // 
            // ключевыеСловаToolStripMenuItem
            // 
            ключевыеСловаToolStripMenuItem.Name = "ключевыеСловаToolStripMenuItem";
            ключевыеСловаToolStripMenuItem.Size = new Size(222, 26);
            ключевыеСловаToolStripMenuItem.Text = "Ключевые слова";
            ключевыеСловаToolStripMenuItem.Click += ключевыеСловаToolStripMenuItem_Click;
            // 
            // добавитьToolStripMenuItem
            // 
            добавитьToolStripMenuItem.DropDownItems.AddRange(new ToolStripItem[] { категориюToolStripMenuItem, продукциюToolStripMenuItem, ключевыеСловаToolStripMenuItem1 });
            добавитьToolStripMenuItem.Font = new Font("Cambria", 14.25F, FontStyle.Regular, GraphicsUnit.Point);
            добавитьToolStripMenuItem.Name = "добавитьToolStripMenuItem";
            добавитьToolStripMenuItem.Size = new Size(104, 26);
            добавитьToolStripMenuItem.Text = "Добавить";
            // 
            // категориюToolStripMenuItem
            // 
            категориюToolStripMenuItem.Name = "категориюToolStripMenuItem";
            категориюToolStripMenuItem.Size = new Size(222, 26);
            категориюToolStripMenuItem.Text = "Категорию";
            категориюToolStripMenuItem.Click += категориюToolStripMenuItem_Click;
            // 
            // продукциюToolStripMenuItem
            // 
            продукциюToolStripMenuItem.Name = "продукциюToolStripMenuItem";
            продукциюToolStripMenuItem.Size = new Size(222, 26);
            продукциюToolStripMenuItem.Text = "Продукцию";
            продукциюToolStripMenuItem.Click += продукциюToolStripMenuItem_Click;
            // 
            // ключевыеСловаToolStripMenuItem1
            // 
            ключевыеСловаToolStripMenuItem1.Name = "ключевыеСловаToolStripMenuItem1";
            ключевыеСловаToolStripMenuItem1.Size = new Size(222, 26);
            ключевыеСловаToolStripMenuItem1.Text = "Ключевые слова";
            ключевыеСловаToolStripMenuItem1.Click += ключевыеСловаToolStripMenuItem1_Click;
            // 
            // редактироватьКатегориюToolStripMenuItem
            // 
            редактироватьКатегориюToolStripMenuItem.Font = new Font("Cambria", 14.25F, FontStyle.Regular, GraphicsUnit.Point);
            редактироватьКатегориюToolStripMenuItem.Name = "редактироватьКатегориюToolStripMenuItem";
            редактироватьКатегориюToolStripMenuItem.Size = new Size(250, 26);
            редактироватьКатегориюToolStripMenuItem.Text = "Редактировать категорию";
            редактироватьКатегориюToolStripMenuItem.Click += редактироватьКатегориюToolStripMenuItem_Click;
            // 
            // редактироватьПродуктToolStripMenuItem
            // 
            редактироватьПродуктToolStripMenuItem.Font = new Font("Cambria", 14.25F, FontStyle.Regular, GraphicsUnit.Point);
            редактироватьПродуктToolStripMenuItem.Name = "редактироватьПродуктToolStripMenuItem";
            редактироватьПродуктToolStripMenuItem.Size = new Size(230, 26);
            редактироватьПродуктToolStripMenuItem.Text = "Редактировать продукт";
            редактироватьПродуктToolStripMenuItem.Click += редактироватьПродуктToolStripMenuItem_Click;
            // 
            // редактироватьКлсловоToolStripMenuItem
            // 
            редактироватьКлсловоToolStripMenuItem.Font = new Font("Cambria", 14.25F, FontStyle.Regular, GraphicsUnit.Point);
            редактироватьКлсловоToolStripMenuItem.Name = "редактироватьКлсловоToolStripMenuItem";
            редактироватьКлсловоToolStripMenuItem.Size = new Size(231, 26);
            редактироватьКлсловоToolStripMenuItem.Text = "Редактировать кл.слово";
            редактироватьКлсловоToolStripMenuItem.Click += редактироватьКлсловоToolStripMenuItem_Click;
            // 
            // dataGridView1
            // 
            dataGridView1.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridView1.Location = new Point(42, 17);
            dataGridView1.Name = "dataGridView1";
            dataGridView1.RowTemplate.Height = 25;
            dataGridView1.Size = new Size(1077, 503);
            dataGridView1.TabIndex = 2;
            // 
            // tabControl1
            // 
            tabControl1.Controls.Add(tabPage1);
            tabControl1.Controls.Add(tabPage2);
            tabControl1.Controls.Add(tabPage5);
            tabControl1.Controls.Add(tabPage3);
            tabControl1.Controls.Add(tabPage4);
            tabControl1.Controls.Add(tabPage6);
            tabControl1.Controls.Add(tabPage7);
            tabControl1.Controls.Add(tabPage8);
            tabControl1.Location = new Point(67, 57);
            tabControl1.Name = "tabControl1";
            tabControl1.SelectedIndex = 0;
            tabControl1.Size = new Size(1172, 569);
            tabControl1.TabIndex = 3;
            // 
            // tabPage1
            // 
            tabPage1.Controls.Add(dataGridView1);
            tabPage1.Font = new Font("Cambria", 12F, FontStyle.Regular, GraphicsUnit.Point);
            tabPage1.Location = new Point(4, 24);
            tabPage1.Name = "tabPage1";
            tabPage1.Padding = new Padding(3);
            tabPage1.Size = new Size(1164, 541);
            tabPage1.TabIndex = 0;
            tabPage1.Text = "Отображение";
            tabPage1.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            tabPage2.Controls.Add(button1);
            tabPage2.Controls.Add(comboBox2);
            tabPage2.Controls.Add(label3);
            tabPage2.Controls.Add(label2);
            tabPage2.Controls.Add(label1);
            tabPage2.Controls.Add(textBox2);
            tabPage2.Controls.Add(textBox1);
            tabPage2.Font = new Font("Cambria", 12F, FontStyle.Regular, GraphicsUnit.Point);
            tabPage2.Location = new Point(4, 24);
            tabPage2.Name = "tabPage2";
            tabPage2.Padding = new Padding(3);
            tabPage2.Size = new Size(1164, 541);
            tabPage2.TabIndex = 1;
            tabPage2.Text = "Добавить категорию";
            tabPage2.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            button1.Font = new Font("Cambria", 18F, FontStyle.Regular, GraphicsUnit.Point);
            button1.Location = new Point(431, 414);
            button1.Name = "button1";
            button1.Size = new Size(328, 77);
            button1.TabIndex = 10;
            button1.Text = "Добавить";
            button1.UseVisualStyleBackColor = true;
            button1.Click += button1_Click;
            // 
            // comboBox2
            // 
            comboBox2.FormattingEnabled = true;
            comboBox2.Location = new Point(359, 309);
            comboBox2.Name = "comboBox2";
            comboBox2.Size = new Size(277, 27);
            comboBox2.TabIndex = 9;
            // 
            // label3
            // 
            label3.Font = new Font("Cambria", 15.75F, FontStyle.Regular, GraphicsUnit.Point);
            label3.Location = new Point(101, 278);
            label3.Name = "label3";
            label3.Size = new Size(238, 82);
            label3.TabIndex = 8;
            label3.Text = "Продукт категории:";
            label3.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // label2
            // 
            label2.Font = new Font("Cambria", 15.75F, FontStyle.Regular, GraphicsUnit.Point);
            label2.Location = new Point(101, 173);
            label2.Name = "label2";
            label2.Size = new Size(238, 82);
            label2.TabIndex = 7;
            label2.Text = "Иконка категории:";
            label2.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // label1
            // 
            label1.Font = new Font("Cambria", 15.75F, FontStyle.Regular, GraphicsUnit.Point);
            label1.Location = new Point(101, 63);
            label1.Name = "label1";
            label1.Size = new Size(238, 82);
            label1.TabIndex = 6;
            label1.Text = "Имя категории:";
            label1.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // textBox2
            // 
            textBox2.BackColor = SystemColors.ButtonHighlight;
            textBox2.Font = new Font("Cambria", 11.25F, FontStyle.Regular, GraphicsUnit.Point);
            textBox2.Location = new Point(359, 202);
            textBox2.Name = "textBox2";
            textBox2.Size = new Size(277, 25);
            textBox2.TabIndex = 4;
            // 
            // textBox1
            // 
            textBox1.BackColor = SystemColors.ButtonHighlight;
            textBox1.Font = new Font("Cambria", 11.25F, FontStyle.Regular, GraphicsUnit.Point);
            textBox1.Location = new Point(359, 91);
            textBox1.Name = "textBox1";
            textBox1.Size = new Size(277, 25);
            textBox1.TabIndex = 3;
            // 
            // tabPage5
            // 
            tabPage5.Controls.Add(textBox6);
            tabPage5.Controls.Add(textBox5);
            tabPage5.Controls.Add(button2);
            tabPage5.Controls.Add(label10);
            tabPage5.Controls.Add(label9);
            tabPage5.Controls.Add(label8);
            tabPage5.Controls.Add(comboBox6);
            tabPage5.Controls.Add(comboBox5);
            tabPage5.Controls.Add(comboBox4);
            tabPage5.Controls.Add(label7);
            tabPage5.Controls.Add(label6);
            tabPage5.Controls.Add(textBox4);
            tabPage5.Controls.Add(textBox3);
            tabPage5.Controls.Add(label4);
            tabPage5.Controls.Add(label5);
            tabPage5.Location = new Point(4, 24);
            tabPage5.Name = "tabPage5";
            tabPage5.Size = new Size(1164, 541);
            tabPage5.TabIndex = 4;
            tabPage5.Text = "Добавить продукт";
            tabPage5.UseVisualStyleBackColor = true;
            // 
            // textBox6
            // 
            textBox6.BackColor = SystemColors.ButtonHighlight;
            textBox6.Font = new Font("Cambria", 11.25F, FontStyle.Regular, GraphicsUnit.Point);
            textBox6.Location = new Point(291, 235);
            textBox6.Name = "textBox6";
            textBox6.Size = new Size(237, 25);
            textBox6.TabIndex = 25;
            // 
            // textBox5
            // 
            textBox5.BackColor = SystemColors.ButtonHighlight;
            textBox5.Font = new Font("Cambria", 11.25F, FontStyle.Regular, GraphicsUnit.Point);
            textBox5.Location = new Point(291, 186);
            textBox5.Name = "textBox5";
            textBox5.Size = new Size(237, 25);
            textBox5.TabIndex = 24;
            // 
            // button2
            // 
            button2.Font = new Font("Cambria", 18F, FontStyle.Regular, GraphicsUnit.Point);
            button2.Location = new Point(734, 343);
            button2.Name = "button2";
            button2.Size = new Size(328, 77);
            button2.TabIndex = 23;
            button2.Text = "Добавить";
            button2.UseVisualStyleBackColor = true;
            button2.Click += button2_Click;
            // 
            // label10
            // 
            label10.Font = new Font("Cambria", 15.75F, FontStyle.Regular, GraphicsUnit.Point);
            label10.Location = new Point(47, 383);
            label10.Name = "label10";
            label10.Size = new Size(238, 38);
            label10.TabIndex = 22;
            label10.Text = "Ключевое слово:";
            label10.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // label9
            // 
            label9.Font = new Font("Cambria", 15.75F, FontStyle.Regular, GraphicsUnit.Point);
            label9.Location = new Point(47, 343);
            label9.Name = "label9";
            label9.Size = new Size(238, 38);
            label9.TabIndex = 21;
            label9.Text = "Категория:";
            label9.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // label8
            // 
            label8.Font = new Font("Cambria", 15.75F, FontStyle.Regular, GraphicsUnit.Point);
            label8.Location = new Point(47, 296);
            label8.Name = "label8";
            label8.Size = new Size(238, 38);
            label8.TabIndex = 20;
            label8.Text = "Корзина:";
            label8.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // comboBox6
            // 
            comboBox6.FormattingEnabled = true;
            comboBox6.Location = new Point(314, 395);
            comboBox6.Name = "comboBox6";
            comboBox6.Size = new Size(204, 23);
            comboBox6.TabIndex = 19;
            // 
            // comboBox5
            // 
            comboBox5.FormattingEnabled = true;
            comboBox5.Location = new Point(314, 355);
            comboBox5.Name = "comboBox5";
            comboBox5.Size = new Size(204, 23);
            comboBox5.TabIndex = 18;
            // 
            // comboBox4
            // 
            comboBox4.FormattingEnabled = true;
            comboBox4.Location = new Point(314, 308);
            comboBox4.Name = "comboBox4";
            comboBox4.Size = new Size(204, 23);
            comboBox4.TabIndex = 17;
            // 
            // label7
            // 
            label7.Font = new Font("Cambria", 15.75F, FontStyle.Regular, GraphicsUnit.Point);
            label7.Location = new Point(47, 225);
            label7.Name = "label7";
            label7.Size = new Size(238, 38);
            label7.TabIndex = 15;
            label7.Text = "Изображение:";
            label7.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // label6
            // 
            label6.Font = new Font("Cambria", 15.75F, FontStyle.Regular, GraphicsUnit.Point);
            label6.Location = new Point(47, 70);
            label6.Name = "label6";
            label6.Size = new Size(238, 38);
            label6.TabIndex = 12;
            label6.Text = "Имя продукта:";
            label6.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // textBox4
            // 
            textBox4.BackColor = SystemColors.ButtonHighlight;
            textBox4.Font = new Font("Cambria", 11.25F, FontStyle.Regular, GraphicsUnit.Point);
            textBox4.Location = new Point(291, 139);
            textBox4.Name = "textBox4";
            textBox4.Size = new Size(237, 25);
            textBox4.TabIndex = 10;
            // 
            // textBox3
            // 
            textBox3.BackColor = SystemColors.ButtonHighlight;
            textBox3.Font = new Font("Cambria", 11.25F, FontStyle.Regular, GraphicsUnit.Point);
            textBox3.Location = new Point(291, 80);
            textBox3.Name = "textBox3";
            textBox3.Size = new Size(237, 25);
            textBox3.TabIndex = 11;
            // 
            // label4
            // 
            label4.Font = new Font("Cambria", 15.75F, FontStyle.Regular, GraphicsUnit.Point);
            label4.Location = new Point(47, 176);
            label4.Name = "label4";
            label4.Size = new Size(238, 38);
            label4.TabIndex = 14;
            label4.Text = "Акционная цена:";
            label4.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // label5
            // 
            label5.Font = new Font("Cambria", 15.75F, FontStyle.Regular, GraphicsUnit.Point);
            label5.Location = new Point(47, 126);
            label5.Name = "label5";
            label5.Size = new Size(238, 38);
            label5.TabIndex = 13;
            label5.Text = "Цена продукта:";
            label5.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // tabPage3
            // 
            tabPage3.Controls.Add(button3);
            tabPage3.Controls.Add(label11);
            tabPage3.Controls.Add(label12);
            tabPage3.Controls.Add(comboBox1);
            tabPage3.Controls.Add(comboBox3);
            tabPage3.Font = new Font("Cambria", 12F, FontStyle.Regular, GraphicsUnit.Point);
            tabPage3.Location = new Point(4, 24);
            tabPage3.Name = "tabPage3";
            tabPage3.Size = new Size(1164, 541);
            tabPage3.TabIndex = 2;
            tabPage3.Text = "Добавить ключевое слово";
            tabPage3.UseVisualStyleBackColor = true;
            // 
            // button3
            // 
            button3.Font = new Font("Cambria", 18F, FontStyle.Regular, GraphicsUnit.Point);
            button3.Location = new Point(362, 362);
            button3.Name = "button3";
            button3.Size = new Size(328, 77);
            button3.TabIndex = 26;
            button3.Text = "Добавить";
            button3.UseVisualStyleBackColor = true;
            button3.Click += button3_Click;
            // 
            // label11
            // 
            label11.Font = new Font("Cambria", 15.75F, FontStyle.Regular, GraphicsUnit.Point);
            label11.Location = new Point(95, 119);
            label11.Name = "label11";
            label11.Size = new Size(238, 38);
            label11.TabIndex = 25;
            label11.Text = "Ключевое слово:";
            label11.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // label12
            // 
            label12.Font = new Font("Cambria", 15.75F, FontStyle.Regular, GraphicsUnit.Point);
            label12.Location = new Point(95, 72);
            label12.Name = "label12";
            label12.Size = new Size(238, 38);
            label12.TabIndex = 24;
            label12.Text = "Продукт:";
            label12.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // comboBox1
            // 
            comboBox1.FormattingEnabled = true;
            comboBox1.Location = new Point(362, 131);
            comboBox1.Name = "comboBox1";
            comboBox1.Size = new Size(204, 27);
            comboBox1.TabIndex = 23;
            // 
            // comboBox3
            // 
            comboBox3.FormattingEnabled = true;
            comboBox3.Location = new Point(362, 84);
            comboBox3.Name = "comboBox3";
            comboBox3.Size = new Size(204, 27);
            comboBox3.TabIndex = 22;
            // 
            // tabPage4
            // 
            tabPage4.Controls.Add(button4);
            tabPage4.Font = new Font("Cambria", 12F, FontStyle.Regular, GraphicsUnit.Point);
            tabPage4.Location = new Point(4, 24);
            tabPage4.Name = "tabPage4";
            tabPage4.Size = new Size(1164, 541);
            tabPage4.TabIndex = 3;
            tabPage4.Text = "Бэкап";
            tabPage4.UseVisualStyleBackColor = true;
            // 
            // button4
            // 
            button4.Font = new Font("Cambria", 24F, FontStyle.Regular, GraphicsUnit.Point);
            button4.Location = new Point(256, 302);
            button4.Name = "button4";
            button4.Size = new Size(569, 122);
            button4.TabIndex = 4;
            button4.Text = "Сделать бэкап";
            button4.UseVisualStyleBackColor = true;
            button4.Click += button4_Click;
            // 
            // tabPage6
            // 
            tabPage6.Controls.Add(comboBox12);
            tabPage6.Controls.Add(label25);
            tabPage6.Controls.Add(label26);
            tabPage6.Controls.Add(button6);
            tabPage6.Controls.Add(comboBox11);
            tabPage6.Controls.Add(label22);
            tabPage6.Controls.Add(label23);
            tabPage6.Controls.Add(label24);
            tabPage6.Controls.Add(textBox10);
            tabPage6.Font = new Font("Cambria", 12F, FontStyle.Regular, GraphicsUnit.Point);
            tabPage6.Location = new Point(4, 24);
            tabPage6.Name = "tabPage6";
            tabPage6.Padding = new Padding(3);
            tabPage6.Size = new Size(1164, 541);
            tabPage6.TabIndex = 5;
            tabPage6.Text = "Редакт. категорию";
            tabPage6.UseVisualStyleBackColor = true;
            // 
            // comboBox12
            // 
            comboBox12.FormattingEnabled = true;
            comboBox12.Location = new Point(827, 135);
            comboBox12.Name = "comboBox12";
            comboBox12.Size = new Size(277, 27);
            comboBox12.TabIndex = 46;
            // 
            // label25
            // 
            label25.AutoSize = true;
            label25.Font = new Font("Cambria", 18F, FontStyle.Bold, GraphicsUnit.Point);
            label25.Location = new Point(74, 53);
            label25.Name = "label25";
            label25.Size = new Size(223, 28);
            label25.TabIndex = 45;
            label25.Text = "Новые параметры";
            // 
            // label26
            // 
            label26.AutoSize = true;
            label26.Font = new Font("Cambria", 18F, FontStyle.Bold, GraphicsUnit.Point);
            label26.Location = new Point(713, 53);
            label26.Name = "label26";
            label26.Size = new Size(382, 28);
            label26.TabIndex = 44;
            label26.Text = "Категория для редактирования";
            // 
            // button6
            // 
            button6.Font = new Font("Cambria", 18F, FontStyle.Regular, GraphicsUnit.Point);
            button6.Location = new Point(583, 407);
            button6.Name = "button6";
            button6.Size = new Size(328, 77);
            button6.TabIndex = 17;
            button6.Text = "Редактировать";
            button6.UseVisualStyleBackColor = true;
            button6.Click += button6_Click;
            // 
            // comboBox11
            // 
            comboBox11.FormattingEnabled = true;
            comboBox11.Location = new Point(295, 244);
            comboBox11.Name = "comboBox11";
            comboBox11.Size = new Size(277, 27);
            comboBox11.TabIndex = 16;
            // 
            // label22
            // 
            label22.Font = new Font("Cambria", 15.75F, FontStyle.Regular, GraphicsUnit.Point);
            label22.Location = new Point(37, 213);
            label22.Name = "label22";
            label22.Size = new Size(238, 82);
            label22.TabIndex = 15;
            label22.Text = "Продукт категории:";
            label22.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // label23
            // 
            label23.Font = new Font("Cambria", 15.75F, FontStyle.Regular, GraphicsUnit.Point);
            label23.Location = new Point(37, 108);
            label23.Name = "label23";
            label23.Size = new Size(238, 82);
            label23.TabIndex = 14;
            label23.Text = "Иконка категории:";
            label23.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // label24
            // 
            label24.Font = new Font("Cambria", 15.75F, FontStyle.Regular, GraphicsUnit.Point);
            label24.Location = new Point(640, 105);
            label24.Name = "label24";
            label24.Size = new Size(238, 82);
            label24.TabIndex = 13;
            label24.Text = "Имя категории:";
            label24.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // textBox10
            // 
            textBox10.BackColor = SystemColors.ButtonHighlight;
            textBox10.Font = new Font("Cambria", 11.25F, FontStyle.Regular, GraphicsUnit.Point);
            textBox10.Location = new Point(295, 137);
            textBox10.Name = "textBox10";
            textBox10.Size = new Size(277, 25);
            textBox10.TabIndex = 12;
            // 
            // tabPage7
            // 
            tabPage7.Controls.Add(label21);
            tabPage7.Controls.Add(label20);
            tabPage7.Controls.Add(comboBox10);
            tabPage7.Controls.Add(button5);
            tabPage7.Controls.Add(textBox7);
            tabPage7.Controls.Add(textBox8);
            tabPage7.Controls.Add(label13);
            tabPage7.Controls.Add(label14);
            tabPage7.Controls.Add(label15);
            tabPage7.Controls.Add(comboBox7);
            tabPage7.Controls.Add(comboBox8);
            tabPage7.Controls.Add(comboBox9);
            tabPage7.Controls.Add(label16);
            tabPage7.Controls.Add(label17);
            tabPage7.Controls.Add(textBox9);
            tabPage7.Controls.Add(label18);
            tabPage7.Controls.Add(label19);
            tabPage7.Font = new Font("Cambria", 12F, FontStyle.Regular, GraphicsUnit.Point);
            tabPage7.Location = new Point(4, 24);
            tabPage7.Name = "tabPage7";
            tabPage7.Size = new Size(1164, 541);
            tabPage7.TabIndex = 6;
            tabPage7.Text = "Редакт. продукт";
            tabPage7.UseVisualStyleBackColor = true;
            // 
            // label21
            // 
            label21.AutoSize = true;
            label21.Font = new Font("Cambria", 18F, FontStyle.Bold, GraphicsUnit.Point);
            label21.Location = new Point(99, 41);
            label21.Name = "label21";
            label21.Size = new Size(223, 28);
            label21.TabIndex = 43;
            label21.Text = "Новые параметры";
            // 
            // label20
            // 
            label20.AutoSize = true;
            label20.Font = new Font("Cambria", 18F, FontStyle.Bold, GraphicsUnit.Point);
            label20.Location = new Point(738, 41);
            label20.Name = "label20";
            label20.Size = new Size(357, 28);
            label20.TabIndex = 42;
            label20.Text = "Продукт для редактирования";
            // 
            // comboBox10
            // 
            comboBox10.FormattingEnabled = true;
            comboBox10.Location = new Point(938, 108);
            comboBox10.Name = "comboBox10";
            comboBox10.Size = new Size(204, 27);
            comboBox10.TabIndex = 41;
            // 
            // button5
            // 
            button5.Font = new Font("Cambria", 18F, FontStyle.Regular, GraphicsUnit.Point);
            button5.Location = new Point(635, 418);
            button5.Name = "button5";
            button5.Size = new Size(328, 77);
            button5.TabIndex = 40;
            button5.Text = "Редактировать";
            button5.UseVisualStyleBackColor = true;
            button5.Click += button5_Click;
            // 
            // textBox7
            // 
            textBox7.BackColor = SystemColors.ButtonHighlight;
            textBox7.Font = new Font("Cambria", 11.25F, FontStyle.Regular, GraphicsUnit.Point);
            textBox7.Location = new Point(306, 206);
            textBox7.Name = "textBox7";
            textBox7.Size = new Size(237, 25);
            textBox7.TabIndex = 39;
            // 
            // textBox8
            // 
            textBox8.BackColor = SystemColors.ButtonHighlight;
            textBox8.Font = new Font("Cambria", 11.25F, FontStyle.Regular, GraphicsUnit.Point);
            textBox8.Location = new Point(306, 157);
            textBox8.Name = "textBox8";
            textBox8.Size = new Size(237, 25);
            textBox8.TabIndex = 38;
            // 
            // label13
            // 
            label13.Font = new Font("Cambria", 15.75F, FontStyle.Regular, GraphicsUnit.Point);
            label13.Location = new Point(62, 354);
            label13.Name = "label13";
            label13.Size = new Size(238, 38);
            label13.TabIndex = 37;
            label13.Text = "Ключевое слово:";
            label13.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // label14
            // 
            label14.Font = new Font("Cambria", 15.75F, FontStyle.Regular, GraphicsUnit.Point);
            label14.Location = new Point(62, 314);
            label14.Name = "label14";
            label14.Size = new Size(238, 38);
            label14.TabIndex = 36;
            label14.Text = "Категория:";
            label14.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // label15
            // 
            label15.Font = new Font("Cambria", 15.75F, FontStyle.Regular, GraphicsUnit.Point);
            label15.Location = new Point(62, 267);
            label15.Name = "label15";
            label15.Size = new Size(238, 38);
            label15.TabIndex = 35;
            label15.Text = "Корзина:";
            label15.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // comboBox7
            // 
            comboBox7.FormattingEnabled = true;
            comboBox7.Location = new Point(329, 366);
            comboBox7.Name = "comboBox7";
            comboBox7.Size = new Size(204, 27);
            comboBox7.TabIndex = 34;
            // 
            // comboBox8
            // 
            comboBox8.FormattingEnabled = true;
            comboBox8.Location = new Point(329, 326);
            comboBox8.Name = "comboBox8";
            comboBox8.Size = new Size(204, 27);
            comboBox8.TabIndex = 33;
            // 
            // comboBox9
            // 
            comboBox9.FormattingEnabled = true;
            comboBox9.Location = new Point(329, 279);
            comboBox9.Name = "comboBox9";
            comboBox9.Size = new Size(204, 27);
            comboBox9.TabIndex = 32;
            // 
            // label16
            // 
            label16.Font = new Font("Cambria", 15.75F, FontStyle.Regular, GraphicsUnit.Point);
            label16.Location = new Point(62, 196);
            label16.Name = "label16";
            label16.Size = new Size(238, 38);
            label16.TabIndex = 31;
            label16.Text = "Изображение:";
            label16.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // label17
            // 
            label17.Font = new Font("Cambria", 15.75F, FontStyle.Regular, GraphicsUnit.Point);
            label17.Location = new Point(654, 99);
            label17.Name = "label17";
            label17.Size = new Size(238, 38);
            label17.TabIndex = 28;
            label17.Text = "Имя продукта:";
            label17.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // textBox9
            // 
            textBox9.BackColor = SystemColors.ButtonHighlight;
            textBox9.Font = new Font("Cambria", 11.25F, FontStyle.Regular, GraphicsUnit.Point);
            textBox9.Location = new Point(306, 110);
            textBox9.Name = "textBox9";
            textBox9.Size = new Size(237, 25);
            textBox9.TabIndex = 26;
            // 
            // label18
            // 
            label18.Font = new Font("Cambria", 15.75F, FontStyle.Regular, GraphicsUnit.Point);
            label18.Location = new Point(62, 147);
            label18.Name = "label18";
            label18.Size = new Size(238, 38);
            label18.TabIndex = 30;
            label18.Text = "Акционная цена:";
            label18.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // label19
            // 
            label19.Font = new Font("Cambria", 15.75F, FontStyle.Regular, GraphicsUnit.Point);
            label19.Location = new Point(62, 97);
            label19.Name = "label19";
            label19.Size = new Size(238, 38);
            label19.TabIndex = 29;
            label19.Text = "Цена продукта:";
            label19.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // tabPage8
            // 
            tabPage8.Controls.Add(button7);
            tabPage8.Controls.Add(label29);
            tabPage8.Controls.Add(label30);
            tabPage8.Controls.Add(comboBox13);
            tabPage8.Controls.Add(comboBox14);
            tabPage8.Controls.Add(label27);
            tabPage8.Controls.Add(label28);
            tabPage8.Font = new Font("Cambria", 12F, FontStyle.Regular, GraphicsUnit.Point);
            tabPage8.Location = new Point(4, 24);
            tabPage8.Name = "tabPage8";
            tabPage8.Size = new Size(1164, 541);
            tabPage8.TabIndex = 7;
            tabPage8.Text = "Редакт. кл.слово";
            tabPage8.UseVisualStyleBackColor = true;
            // 
            // button7
            // 
            button7.Font = new Font("Cambria", 18F, FontStyle.Regular, GraphicsUnit.Point);
            button7.Location = new Point(405, 333);
            button7.Name = "button7";
            button7.Size = new Size(328, 77);
            button7.TabIndex = 52;
            button7.Text = "Редактировать";
            button7.UseVisualStyleBackColor = true;
            button7.Click += button7_Click;
            // 
            // label29
            // 
            label29.Font = new Font("Cambria", 15.75F, FontStyle.Regular, GraphicsUnit.Point);
            label29.Location = new Point(642, 141);
            label29.Name = "label29";
            label29.Size = new Size(238, 38);
            label29.TabIndex = 51;
            label29.Text = "Ключевое слово:";
            label29.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // label30
            // 
            label30.Font = new Font("Cambria", 15.75F, FontStyle.Regular, GraphicsUnit.Point);
            label30.Location = new Point(66, 141);
            label30.Name = "label30";
            label30.Size = new Size(238, 38);
            label30.TabIndex = 50;
            label30.Text = "Продукт:";
            label30.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // comboBox13
            // 
            comboBox13.FormattingEnabled = true;
            comboBox13.Location = new Point(910, 150);
            comboBox13.Name = "comboBox13";
            comboBox13.Size = new Size(204, 27);
            comboBox13.TabIndex = 49;
            // 
            // comboBox14
            // 
            comboBox14.FormattingEnabled = true;
            comboBox14.Location = new Point(333, 153);
            comboBox14.Name = "comboBox14";
            comboBox14.Size = new Size(204, 27);
            comboBox14.TabIndex = 48;
            // 
            // label27
            // 
            label27.AutoSize = true;
            label27.Font = new Font("Cambria", 18F, FontStyle.Bold, GraphicsUnit.Point);
            label27.Location = new Point(81, 70);
            label27.Name = "label27";
            label27.Size = new Size(223, 28);
            label27.TabIndex = 47;
            label27.Text = "Новые параметры";
            // 
            // label28
            // 
            label28.AutoSize = true;
            label28.Font = new Font("Cambria", 18F, FontStyle.Bold, GraphicsUnit.Point);
            label28.Location = new Point(720, 70);
            label28.Name = "label28";
            label28.Size = new Size(363, 28);
            label28.TabIndex = 46;
            label28.Text = "Кл.слово для редактирования";
            // 
            // openFileDialog1
            // 
            openFileDialog1.FileName = "openFileDialog1";
            // 
            // Form1
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(1351, 663);
            Controls.Add(tabControl1);
            Controls.Add(menuStrip1);
            MainMenuStrip = menuStrip1;
            Name = "Form1";
            Text = "123";
            Load += Form1_Load;
            menuStrip1.ResumeLayout(false);
            menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)dataGridView1).EndInit();
            tabControl1.ResumeLayout(false);
            tabPage1.ResumeLayout(false);
            tabPage2.ResumeLayout(false);
            tabPage2.PerformLayout();
            tabPage5.ResumeLayout(false);
            tabPage5.PerformLayout();
            tabPage3.ResumeLayout(false);
            tabPage4.ResumeLayout(false);
            tabPage6.ResumeLayout(false);
            tabPage6.PerformLayout();
            tabPage7.ResumeLayout(false);
            tabPage7.PerformLayout();
            tabPage8.ResumeLayout(false);
            tabPage8.PerformLayout();
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private ContextMenuStrip contextMenuStrip1;
        private MenuStrip menuStrip1;
        private ToolStripMenuItem отобразитьToolStripMenuItem;
        private ToolStripMenuItem категорииToolStripMenuItem;
        private ToolStripMenuItem продуктыToolStripMenuItem;
        private ToolStripMenuItem ключевыеСловаToolStripMenuItem;
        private ToolStripMenuItem добавитьToolStripMenuItem;
        private ToolStripMenuItem категориюToolStripMenuItem;
        private ToolStripMenuItem продукциюToolStripMenuItem;
        private ToolStripMenuItem ключевыеСловаToolStripMenuItem1;
        private DataGridView dataGridView1;
        private TabControl tabControl1;
        private TabPage tabPage1;
        private TabPage tabPage2;
        private TabPage tabPage3;
        private TabPage tabPage4;
        private TextBox textBox2;
        private TextBox textBox1;
        private Label label3;
        private Label label2;
        private Label label1;
        private ComboBox comboBox2;
        private Button button1;
        private TabPage tabPage5;
        private Label label6;
        private TextBox textBox4;
        private TextBox textBox3;
        private Label label4;
        private Label label5;
        private Label label7;
        private ComboBox comboBox5;
        private ComboBox comboBox4;
        private Label label10;
        private Label label9;
        private Label label8;
        private ComboBox comboBox6;
        private Button button2;
        private TextBox textBox6;
        private TextBox textBox5;
        private Button button3;
        private Label label11;
        private Label label12;
        private ComboBox comboBox1;
        private ComboBox comboBox3;
        private Button button4;
        private OpenFileDialog openFileDialog1;
        private ToolStripMenuItem редактироватьКатегориюToolStripMenuItem;
        private ToolStripMenuItem редактироватьПродуктToolStripMenuItem;
        private ToolStripMenuItem редактироватьКлсловоToolStripMenuItem;
        private TabPage tabPage6;
        private TabPage tabPage7;
        private Label label21;
        private Label label20;
        private ComboBox comboBox10;
        private Button button5;
        private TextBox textBox7;
        private TextBox textBox8;
        private Label label13;
        private Label label14;
        private Label label15;
        private ComboBox comboBox7;
        private ComboBox comboBox8;
        private ComboBox comboBox9;
        private Label label16;
        private Label label17;
        private TextBox textBox9;
        private Label label18;
        private Label label19;
        private TabPage tabPage8;
        private Label label25;
        private Label label26;
        private Button button6;
        private ComboBox comboBox11;
        private Label label22;
        private Label label23;
        private Label label24;
        private TextBox textBox10;
        private ComboBox comboBox12;
        private Button button7;
        private Label label29;
        private Label label30;
        private ComboBox comboBox13;
        private ComboBox comboBox14;
        private Label label27;
        private Label label28;
    }
}