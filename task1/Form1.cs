using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System.Windows.Forms;
using task1.Models;
using static System.Net.Mime.MediaTypeNames;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.TextBox;

namespace task1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            using (WebshopContext db = new WebshopContext())
            {
                var products = db.Products;

                foreach (var item in products)
                {
                    comboBox2.Items.Add(item.Name);
                    comboBox3.Items.Add(item.Name);
                    comboBox10.Items.Add(item.Name);
                    comboBox11.Items.Add(item.Name);
                    comboBox14.Items.Add(item.Name);
                }
            }

            using (WebshopContext db = new WebshopContext())
            {
                var carts = db.Carts;

                foreach (var item in carts)
                {
                    comboBox4.Items.Add(item.Id);
                    comboBox9.Items.Add(item.Id);
                }
            }

            using (WebshopContext db = new WebshopContext())
            {
                var categories = db.Categories;

                foreach (var item in categories)
                {
                    comboBox5.Items.Add(item.Name);
                    comboBox8.Items.Add(item.Name);
                    comboBox12.Items.Add(item.Name);
                }
            }

            using (WebshopContext db = new WebshopContext())
            {
                var keylinks = db.KeyLinks.Include(kl => kl.KeyWord);

                foreach (var item in keylinks)
                {
                    comboBox6.Items.Add(item.KeyWord.Keyword);
                    comboBox1.Items.Add(item.KeyWord.Keyword);
                    comboBox7.Items.Add(item.KeyWord.Keyword);
                    comboBox13.Items.Add(item.KeyWord.Keyword);
                }
            }
        }

        private void ���������ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            dataGridView1.Rows.Clear();
            dataGridView1.Columns.Clear();

            tabControl1.SelectedTab = tabControl1.TabPages["tabPage1"];

            using (WebshopContext db = new WebshopContext())
            {
                var allCategories = db.Categories.Include(c => c.Products).ToList();

                DataGridViewTextBoxColumn category = new DataGridViewTextBoxColumn();
                category.HeaderText = "Category";

                DataGridViewTextBoxColumn icon = new DataGridViewTextBoxColumn();
                icon.HeaderText = "Icon";

                DataGridViewTextBoxColumn products = new DataGridViewTextBoxColumn();
                products.HeaderText = "Products";
                products.Width = 250;

                dataGridView1.Columns.Add(category);
                dataGridView1.Columns.Add(icon);
                dataGridView1.Columns.Add(products);

                int i = 0;
                foreach (var item in allCategories)
                {
                    string text = "";

                    foreach (var product in item.Products)
                    {
                        text += product.Name + "; ";
                    }
                    dataGridView1.Rows.Add
                    (
                        dataGridView1.Rows[i].Cells[0].Value = item.Name,
                        dataGridView1.Rows[i].Cells[1].Value = item.Icon,
                        dataGridView1.Rows[i].Cells[2].Value = text
                    );
                    i++;
                }
            }
        }

        private void ��������ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            dataGridView1.Rows.Clear();
            dataGridView1.Columns.Clear();

            tabControl1.SelectedTab = tabControl1.TabPages["tabPage1"];

            using (WebshopContext db = new WebshopContext())
            {
                var products = db.Products.Include(p => p.Category)
                    .Include(p => p.KeyLinks)
                    .ThenInclude(kl => kl.KeyWord)
                    .ToList();

                DataGridViewTextBoxColumn name = new DataGridViewTextBoxColumn();
                name.HeaderText = "Name";

                DataGridViewTextBoxColumn price = new DataGridViewTextBoxColumn();
                price.HeaderText = "Price";

                DataGridViewTextBoxColumn category = new DataGridViewTextBoxColumn();
                category.HeaderText = "Category";

                DataGridViewTextBoxColumn keyword = new DataGridViewTextBoxColumn();
                keyword.HeaderText = "Key words";
                keyword.Width = 200;

                dataGridView1.Columns.Add(name);
                dataGridView1.Columns.Add(price);
                dataGridView1.Columns.Add(category);
                dataGridView1.Columns.Add(keyword);

                int i = 0;
                foreach (var item in products)
                {
                    string text = "";

                    foreach (var kw in item.KeyLinks)
                    {
                        text += kw.KeyWord.Keyword + "; ";
                    }
                    dataGridView1.Rows.Add
                    (
                        dataGridView1.Rows[i].Cells[0].Value = item.Name,
                        dataGridView1.Rows[i].Cells[1].Value = item.Price,
                        dataGridView1.Rows[i].Cells[2].Value = item.Category.Name,
                        dataGridView1.Rows[i].Cells[3].Value = text
                    );
                    i++;
                }
            }
        }

        private void �������������ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            dataGridView1.Rows.Clear();
            dataGridView1.Columns.Clear();

            tabControl1.SelectedTab = tabControl1.TabPages["tabPage1"];

            using (WebshopContext db = new WebshopContext())
            {
                var keylinks = db.KeyLinks.Include(kl => kl.KeyWord)
                    .Include(kl => kl.Product)
                    .ToList();

                DataGridViewTextBoxColumn productName = new DataGridViewTextBoxColumn();
                productName.HeaderText = "Product name";
                productName.Width = 100;

                DataGridViewTextBoxColumn keyword = new DataGridViewTextBoxColumn();
                keyword.HeaderText = "Key word";
                keyword.Width = 200;

                dataGridView1.Columns.Add(productName);
                dataGridView1.Columns.Add(keyword);

                int i = 0;
                foreach (var item in keylinks)
                {
                    dataGridView1.Rows.Add
                    (
                        dataGridView1.Rows[i].Cells[0].Value = item.Product.Name,
                        dataGridView1.Rows[i].Cells[1].Value = item.KeyWord.Keyword
                    );
                    i++;
                }
            }
        }

        private void ���������ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            tabControl1.SelectedTab = tabControl1.TabPages["tabPage2"];
        }

        private void button1_Click(object sender, EventArgs e)
        {
            using (WebshopContext db = new WebshopContext())
            {
                try
                {
                    if (textBox1.Text.Length != 0 && textBox1.Text.Length != 0)
                    {
                        var product = db.Products.FirstOrDefault(p => p.Name == comboBox2.SelectedItem.ToString());

                        Category newCategory = new Category(textBox1.Text, textBox2.Text);

                        if (product != null)
                        {
                            newCategory.Products.Add(product);
                            product.Category = newCategory;
                        }

                        db.Categories.Add(newCategory);
                        db.SaveChanges();
                    }
                }
                catch
                {
                    MessageBox.Show("������! ��������� ������");
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            using (WebshopContext db = new WebshopContext())
            {
                try
                {
                    if (textBox3.Text.Length != 0 && textBox4.Text.Length != 0 && textBox5.Text.Length != 0 && textBox6.Text.Length != 0)
                    {
                        var cart = db.Carts.FirstOrDefault(c => c.Id.ToString() == comboBox4.SelectedItem.ToString());
                        var category = db.Categories.FirstOrDefault(p => p.Name == comboBox5.SelectedItem.ToString());
                        var word = db.Words.FirstOrDefault(w => w.Keyword == comboBox6.SelectedItem.ToString());

                        Product newProduct = new Product(textBox3.Text, Convert.ToDouble(textBox4.Text), Convert.ToDouble(textBox5.Text), textBox5.Text);

                        if (cart != null && category != null && word != null)
                        {
                            newProduct.Carts.Add(cart);
                            newProduct.KeyLinks.Add(db.KeyLinks.FirstOrDefault(kl => kl.KeyWord == word));
                            newProduct.CategoryId = category.Id;
                        }

                        db.Products.Add(newProduct);
                        db.SaveChanges();
                    }
                }
                catch
                {
                    MessageBox.Show("������! ��������� ������");
                }
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            using (WebshopContext db = new WebshopContext())
            {
                try
                {
                    var product = db.Products.FirstOrDefault(c => c.Id.ToString() == comboBox3.SelectedItem.ToString());
                    var word = db.Words.FirstOrDefault(w => w.Keyword == comboBox1.SelectedItem.ToString());

                    if (product != null && word != null)
                    {
                        product.KeyLinks.Add(db.KeyLinks.FirstOrDefault(k => k.KeyWord == word));
                    }

                    db.SaveChanges();

                }
                catch
                {
                    MessageBox.Show("������! ��������� ������");
                }
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = "���� ������ (*.mdf; *.ldf)|*.mdf; *.ldf";
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                string path = dialog.FileName;

                SqlConnection connection = new SqlConnection("Data Source=localhost;Initial Catalog=Webshop;Integrated Security=True");

                SqlCommand command = new SqlCommand("BACKUP DATABASE Webshop TO DISK = @path", connection);
                command.Parameters.AddWithValue("@path", path);

                connection.Open();
                command.ExecuteNonQuery();
                connection.Close();
            }
        }

        private void ���������ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            tabControl1.SelectedTab = tabControl1.TabPages["tabPage5"];
        }

        private void �������������ToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            tabControl1.SelectedTab = tabControl1.TabPages["tabPage3"];
        }

        private void ����������������������ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            tabControl1.SelectedTab = tabControl1.TabPages["tabPage6"];
        }

        private void ��������������������ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            tabControl1.SelectedTab = tabControl1.TabPages["tabPage7"];
        }

        private void ��������������������ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            tabControl1.SelectedTab = tabControl1.TabPages["tabPage8"];
        }


        private void button5_Click(object sender, EventArgs e)
        {
            using (WebshopContext db = new WebshopContext())
            {
                try
                {
                    var category = db.Categories.FirstOrDefault(c => c.Name == comboBox12.SelectedItem.ToString());
                    var product = db.Products.FirstOrDefault(p => p.Name == comboBox11.SelectedItem.ToString());
                    var cart = db.Carts.FirstOrDefault(c => c.Id.ToString() == comboBox9.SelectedItem.ToString());
                    var word = db.Words.FirstOrDefault(w => w.Keyword == comboBox7.SelectedItem.ToString());

                    if (product != null)
                    {
                        product.Price = Convert.ToDouble(textBox9.Text);
                        product.ActionPrice = Convert.ToDouble(textBox8.Text);
                        product.ImageUrl = comboBox7.Text;
                        product.Category = category;
                        product.KeyLinks.Add(db.KeyLinks.FirstOrDefault(kl => kl.KeyWord == word));
                    }

                    db.SaveChanges();
                }
                catch
                {
                    MessageBox.Show("������! ��������� ������");
                }
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            using (WebshopContext db = new WebshopContext())
            {
                try
                {
                    var product = db.Products.FirstOrDefault(p => p.Name == comboBox11.SelectedItem.ToString());
                    var category = db.Categories.FirstOrDefault(c => c.Name == comboBox12.SelectedItem.ToString());

                    if (category != null && product != null)
                    {
                        category.Icon = textBox10.Text;
                        category.Products.Add(product);
                    }

                    db.SaveChanges();
                }
                catch
                {
                    MessageBox.Show("������! ��������� ������");
                }
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            using (WebshopContext db = new WebshopContext())
            {
                try
                {
                    var product = db.Products.FirstOrDefault(p => p.Name == comboBox14.SelectedItem.ToString());
                    var keyword = db.Categories.FirstOrDefault(c => c.Name == comboBox13.SelectedItem.ToString());

                    if (keyword != null && product != null)
                    {
                       keyword.Products.Add(product);
                    }

                    db.SaveChanges();
                }
                catch
                {
                    MessageBox.Show("������! ��������� ������");
                }
            }
        }
    }
}