﻿using System;
using System.Collections.Generic;

namespace task1.Models;

public partial class KeyLink
{
    public Guid Id { get; set; }

    public Guid ProductId { get; set; }

    public Guid KeyWordId { get; set; }

    public virtual Word KeyWord { get; set; } = null!;

    public virtual Product Product { get; set; } = null!;
}
