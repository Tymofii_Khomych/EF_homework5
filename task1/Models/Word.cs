﻿using System;
using System.Collections.Generic;

namespace task1.Models;

public partial class Word
{
    public Guid Id { get; set; }

    public string Header { get; set; } = null!;

    public string Keyword { get; set; } = null!;

    public virtual ICollection<KeyLink> KeyLinks { get; set; } = new List<KeyLink>();
}
