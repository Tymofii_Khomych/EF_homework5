﻿using System;
using System.Collections.Generic;

namespace task1.Models;

public partial class Category
{
    public Guid Id { get; set; }

    public string Name { get; set; } = null!;

    public string Icon { get; set; } = null!;

    public virtual ICollection<Product> Products { get; set; } = new List<Product>();

    public Category() { }
    public Category(string name, string icon)
    {
        Id = Guid.NewGuid();
        Name = name;
        Icon = icon;
    }
}
