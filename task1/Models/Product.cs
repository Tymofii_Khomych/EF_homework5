﻿using System;
using System.Collections.Generic;

namespace task1.Models;

public partial class Product
{
    public Guid Id { get; set; }

    public string Name { get; set; } = null!;

    public double Price { get; set; }

    public double ActionPrice { get; set; }

    public string? DescriptionField1 { get; set; }

    public string? DescriptionField2 { get; set; }

    public string ImageUrl { get; set; } = null!;

    public Guid CategoryId { get; set; }

    public virtual ICollection<Cart> Carts { get; set; } = new List<Cart>();

    public virtual Category Category { get; set; } = null!;

    public virtual ICollection<KeyLink> KeyLinks { get; set; } = new List<KeyLink>();

    public Product(string name, double price, double actionPrice, string imageUrl)
    {
        Id = Guid.NewGuid();
        Name = name;
        Price = price;
        ActionPrice = actionPrice;
        ImageUrl = imageUrl;
    }

}
